+++
date = "2014-09-28T07:16:56-04:00"
draft = false
title = "Getting a hang of hakyll"

+++
Okay so I figured for my first post for this "blog" I would write a few things on why I chose Hakyll, the site genrator for which this site is built with.

First, I have frequently used [Jekyll](http://jekyllrb.com/) for most of my projects that require a web page.  It works fairly well and I know the ins and outs of it.  So despite how much I like jekyll, and how well it works for me I always am looking to change something up or learn something new.  And here we come across [Hakyll](http://jaspervdj.be/hakyll/), like Jekyll it is a static site generator just written in Haskell instead of Ruby, and uses pandoc for the document conversion.

Pandoc is really a great piece of software, allowing you to convert just about any format of text into another.  This way I am writing this blogpost in markdown, but pandoc converts it to html upon rendering.

So I set out to try hakyll and ran into a few problems.  One is that cabal, the package manager for Haskell is really out of date on Debian (my typical server OS of choice) and cabal basically compiles the software on the machine for use.  The issue is a 256Mb VPS is not going to have enough RAM for this compiling process and OOB kills the process before it comes even close to completing.  So what I did was load cabal onto a VM at home, and then wrote a simple bash script for "pushing" the finished compiled site to my nfalcone.info vps.


So on the VM at home/ your machine with enough RAM to install hakyll:

```
sudo apt-get install cabal-install
sudo cabal update
sudo cabal install cabal-install --global
```

What the above does is basically bootstraps an install for a newer version of cabal with the old version packaged by debian, if you plan on using Hakyll on a different platform, like OSX you probably will not have to mess around with this.


Then just a simple

```
sudo cabal install hakyll --global
```

Sometimes I have had to install pandoc first prior to hakyll, as it seems to get hung up on the wrong version for some reason.  



Now that I have hakyll installed I can finally build my site with:

```
hakyll-init nfalcone.info
cd nfalcone.info
ghc --make -threaded site.hs
./site build
```

Now that is all fun to type out but when I want to edit and update my site I really do not want to type all that out and still have to upload it via ssh.  So I built a cheap little script to do it, note that the VM I build the site on connects to the VPS that hosts the site via authorized ssh pubkey.

<script src="https://gist.github.com/nfalcone/889a536bd25809daf7cd.js"></script>

feel free to use or tweak or whatever, it just simplifies the whole process.


After playing with Hakyll it seems really powerful, and more tweaking and working with will only tell if I start using Hakyll over Jekyll.  Anyway that whole process should get hakyll running for you on a Debian 7 box, the VM I use to compile hakyll and the site only ahs about 1Gb of RAM so you really do not need a crazy VPS/VM to do this in but for a static site I really did not want to purchase one with 1Gb of RAM.
