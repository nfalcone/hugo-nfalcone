+++
draft = false
title = "Nicholas Falcone 2016 Resume"

+++


<html>
  <head>
    <title>NickFalcone.me ~ Resume</title>
    <script type="text/javascript" src="pdfobject.js"></script>
    <script type="text/javascript">
      window.onload = function (){
        var success = new PDFObject({ url: "/resume/resume.pdf" }).embed();
      };
    </script>
  </head> 
  <body>
    <p>It appears you don't have PDF support in this web
    browser. <a href="/resume/resume.pdf">Click here to download the PDF</a></p>
  </body>
</html>
